local relic_bmt, choices, relic_index = nil, nil, 1
local mpn = GAMESTATE:GetMasterPlayerNumber()
local pn = ToEnumShortString(mpn)

local a = Def.Actor{}

a.OnCommand=function(self)
	relic_bmt = SCREENMAN:GetTopScreen():GetOptionRow(1):GetChild(""):GetChild("Item")
end

a.UpdateRelicToCancelChoicesMessageCommand=function(self, list)
	choices = list
	relic_bmt:settext(list[1])
	relic_index = 1
end

a["MenuLeft" .. pn .. "MessageCommand"]=function(self)
	local row_index = SCREENMAN:GetTopScreen():GetCurrentRowIndex(mpn)+1
	if row_index == 2 then
		relic_index = (relic_index-1) > 0 and (relic_index-1) or #choices
		relic_bmt:settext( choices[relic_index] )
	end
end

a["MenuRight" .. pn .. "MessageCommand"]=function(self)
	local row_index = SCREENMAN:GetTopScreen():GetCurrentRowIndex(mpn)+1
	if row_index == 2 then
		relic_index = (relic_index+1) <= #choices and relic_index+1 or 1
		relic_bmt:settext( choices[relic_index] )
	end
end


local UndoAstralRing = function()
	SL.Global.ActiveModifiers.DecentsWayOffs = "On"
	PREFSMAN:SetPreference("TimingWindowSecondsW4", SL.Preferences.Competitive.TimingWindowSecondsW4)
	PREFSMAN:SetPreference("TimingWindowSecondsW5", SL.Preferences.Competitive.TimingWindowSecondsW5)
end

local UndoProtectRing = function()
	local player_state = GAMESTATE:GetPlayerState(GAMESTATE:GetMasterPlayerNumber())
	if player_state then
		local po = player_state:GetPlayerOptions("ModsLevel_Preferred")
		if po then
			po:FailSetting('FailType_ImmediateContinue')
		end
	end
end

local UndoPendulumBlade = function()
	PREFSMAN:SetPreference("LifeDifficultyScale", 1)
end

a.OffCommand=function(self)
	local player_index = SCREENMAN:GetTopScreen():GetOptionRow(0):GetChoiceInRowWithFocus(mpn)
	if player_index == 0 then return end

	local player_to_use_oghma = CustomOptionRow('Staminadventurer').Choices[player_index+1]

	local relic_to_cancel = choices[relic_index]

	for relic in ivalues(ECS.Player.Relics) do
		if relic.name == relic_to_cancel then

			if relic.name == "Champion Belt" or relic.name == "Order of Ambrosia" then
				local extra_relic = choices[relic_index+1]
				if extra_relic then
					if extra_relic.name == "Astral Ring" then UndoAstralRing() end
					if extra_relic.name == "Protect Ring" then UndoProtectRing() end
					if extra_relic.name == "Pendulum Blade" then UndoPendulumBlade() end

					for _relic in ivalues(ECS.Player.Relics) do
						if extra_relic == _relic.name then
							_relic.Oghma = player_to_use_oghma
							break
						end
					end
				end
			end

			if relic.name == "Order of Ambrosia" then
				local extra_relic2 = choices[relic_index+2]
				if extra_relic2 then
					if extra_relic2.name == "Astral Ring" then UndoAstralRing() end
					if extra_relic2.name == "Protect Ring" then UndoProtectRing() end
					if extra_relic2.name == "Pendulum Blade" then UndoPendulumBlade() end

					for _relic in ivalues(ECS.Player.Relics) do
						if extra_relic2 == _relic.name then
							_relic.Oghma = player_to_use_oghma
							break
						end
					end
				end
			end

			if relic.name == "Astral Ring" then UndoAstralRing() end
			if relic.name == "Protect Ring" then UndoProtectRing() end
			if relic.name == "Pendulum Blade" then UndoPendulumBlade() end

			relic.Oghma = player_to_use_oghma
		end
	end
end

return a