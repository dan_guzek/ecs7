return Def.Actor{
	OffCommand=function(self)
		self:sleep(0.9)

		if ECS.Mode == "ECS7" then
			local life_is_a_beach = SONGMAN:FindSong("ECS7/[17] [175] Life is a Beach")
			if life_is_a_beach then
				GAMESTATE:SetPreferredSong(life_is_a_beach)
			end
		end
	end
}