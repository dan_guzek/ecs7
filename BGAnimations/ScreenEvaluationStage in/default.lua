local statsP1 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1);
local statsP2 = STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2);

local gradeP1 = statsP1:GetGrade();
local gradeP2 = statsP2:GetGrade();

local function failed(g)
	return (g == "Grade_Failed")
end

local ApplyRelicActions = function()
	for active_relic in ivalues(ECS.Player.Relics) do
		active_relic.action()
	end
end


local img = "cleared text.png"

-- if (only P1) and (P1 failed)
if ((GAMESTATE:IsHumanPlayer(PLAYER_1) and failed(gradeP1) and not GAMESTATE:IsHumanPlayer(PLAYER_2)))
-- if (only P2) and (P2 failed)
or ((GAMESTATE:IsHumanPlayer(PLAYER_2) and failed(gradeP2) and not GAMESTATE:IsHumanPlayer(PLAYER_1)))
-- if (both P1 and P2) and (both P1 and P2 failed)
or (GAMESTATE:IsHumanPlayer(PLAYER_1) and GAMESTATE:IsHumanPlayer(PLAYER_2) and failed(gradeP1) and failed(gradeP2)) then
	img = "failed text.png"
end


local t = Def.ActorFrame {
	InitCommand=cmd(xy,_screen.cx, _screen.cy),
	OnCommand=function(self) ApplyRelicActions() end,
	OffCommand=function(self)
		-- always undo the effects of Astral Ring when leaving ScreenEval, even if it wasn't active
		SL.Global.ActiveModifiers.DecentsWayOffs = "On"
		PREFSMAN:SetPreference("TimingWindowSecondsW4", SL.Preferences.Competitive.TimingWindowSecondsW4)
		PREFSMAN:SetPreference("TimingWindowSecondsW5", SL.Preferences.Competitive.TimingWindowSecondsW5)

		-- always undo the effects of Protect Ring when leaving ScreenEval, even if it wasn't active
		local player_state = GAMESTATE:GetPlayerState(GAMESTATE:GetMasterPlayerNumber())
		if player_state then
			local po = player_state:GetPlayerOptions("ModsLevel_Preferred")
			if po then
				po:FailSetting('FailType_ImmediateContinue')
			end
		end

		-- always undo the effect of Pendulum Blade when leaving ScreenEval, even if it wasn't active
		PREFSMAN:SetPreference("LifeDifficultyScale", 1)
	end,

	Def.Quad{
		InitCommand=cmd(zoomto,_screen.w,_screen.h; diffuse,color("0,0,0,1"););
		OnCommand=cmd(sleep,0.2; linear,0.5;  diffusealpha,0);
	};

	LoadActor(img)..{
		InitCommand=cmd(zoom,0.8; diffusealpha,0;);
		OnCommand=cmd(accelerate,0.4;diffusealpha,1; sleep,1.3; decelerate,0.4;diffusealpha,0);
	}
}

return t