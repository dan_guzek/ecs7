local path =  THEME:GetThemeDisplayName() .. " UserPrefs.ini"

-- Hook called during profile load
function LoadProfileCustom(profile, dir)

	local fullFilename =  dir .. path
	local pn

	-- we've been passed a profile object as the variable "profile"
	-- see if it matches against anything returned by PROFILEMAN:GetProfile(player)
	for player in ivalues( GAMESTATE:GetHumanPlayers() ) do
		if profile == PROFILEMAN:GetProfile(player) then
			pn = ToEnumShortString(player)
			break
		end
	end

	if pn and FILEMAN:DoesFileExist(fullFilename) then
		SL[pn].ActiveModifiers = IniFile.ReadFile(fullFilename)[THEME:GetThemeDisplayName()]
	end

	local relic_file_path = dir .. "Player_Relic_Data.lua"

	if FILEMAN:DoesFileExist(relic_file_path) then
		local relic_data = LoadActor(relic_file_path)
		if relic_data then
			ECS.Players[PROFILEMAN:GetPlayerName(GAMESTATE:GetMasterPlayerNumber())].relics = relic_data
		end
	end

	return true
end

-- Hook called during profile save
function SaveProfileCustom(profile, dir)

	local fullFilename =  dir .. path

	for player in ivalues( GAMESTATE:GetHumanPlayers() ) do
		if profile == PROFILEMAN:GetProfile(player) then
			local pn = ToEnumShortString(player)
			IniFile.WriteFile( fullFilename, {[THEME:GetThemeDisplayName()]=SL[pn].ActiveModifiers  } )
			break
		end
	end

	return true
end